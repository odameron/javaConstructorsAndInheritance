/**
 * 
 */
package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MyClassWithOneConstructor {

	public MyClassWithOneConstructor(int v) {
		System.out.println("*\tMyClassWithOneConstructor(int) constructor(int)");
	}

	public static void main(String[] args) {
		/*
		 * Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 * The constructor MyClassWithOneConstructor() is undefined
		 */
		//MyClassWithOneConstructor myInstance = new MyClassWithOneConstructor();
		
		System.out.println();
		MyClassWithOneConstructor myOtherInstance = new MyClassWithOneConstructor(3);
	}

}
