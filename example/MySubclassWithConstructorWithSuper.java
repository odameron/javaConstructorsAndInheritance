/**
 * 
 */
package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MySubclassWithConstructorWithSuper extends MyClass {

	
	public MySubclassWithConstructorWithSuper() {
		// call to super() implicit
		System.out.println("**\tMySubclassWithConstructorWithSuper() constructor");
	}
	
	
	public MySubclassWithConstructorWithSuper(int v) {
		/*
		 * here the explicit call to super(v) replaces
		 * the implicit call to super()
		 */
		super(v);
		System.out.println("**\tMySubclassWithConstructorWithSuper(int) constructor");
	}


	public static void main(String[] args) {
		MySubclassWithConstructorWithSuper myInstance = new MySubclassWithConstructorWithSuper();
		System.out.println();
		MySubclassWithConstructorWithSuper myOtherInstance = new MySubclassWithConstructorWithSuper(3);

	}

}
