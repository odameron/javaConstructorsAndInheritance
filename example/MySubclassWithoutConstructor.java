/**
 * 
 */
package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MySubclassWithoutConstructor extends MyClass {


	public static void main(String[] args) {
		
		/*
		 * No constructor provided for MySubclassWithoutConstructor
		 * 
		 * - Java provides a default empty constructor without parameter equivalent to 
		 *   public MySubclassWithoutConstructor() {}
		 * - this constructor automatically calls the constructor without parameter of the superclass 
		 *   (regardless of whether this constructor exists or is the default one)
		 */
		MySubclassWithoutConstructor myInstance = new MySubclassWithoutConstructor();
		System.out.println();
		
		/*
		 *  Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 *  The constructor MySubclassWithoutConstructor(int) is undefined
		 */
		//MySubclassWithoutConstructor myOtherInstance = new MySubclassWithoutConstructor(3);

	}

}
