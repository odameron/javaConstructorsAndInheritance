/**
 * 
 */
package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MyClassWithoutConstructor {

	public static void main(String[] args) {
		MyClassWithoutConstructor myInstance = new MyClassWithoutConstructor();
		System.out.println();
		
		/*
		 * Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 * The constructor MyClassWithoutConstructor(int) is undefined
		 */
		//MyClassWithoutConstructor myOtherInstance = new MyClassWithoutConstructor(3);
	}

}
