package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MyClass {

	/**
	 * Default constructor without parameter
	 */
	public MyClass() {
		System.out.println("*\tMyClass() constructor");
	}
	
	
	/**
	 * Constructor with integer parameter
	 * 
	 * @param int v
	 */
	public MyClass(int v) {
		System.out.println("*\tMyClass(int) constructor(int)");
	}


	public static void main(String[] args) {
		MyClass myInstance = new MyClass();
		System.out.println();
		MyClass myOtherInstance = new MyClass(3);
	}

}
