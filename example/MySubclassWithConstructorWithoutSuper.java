/**
 * 
 */
package javaConstructorsAndInheritance;

/**
 * @author Olivier Dameron
 * @version 1.0
 *
 */
public class MySubclassWithConstructorWithoutSuper extends MyClass {

	public MySubclassWithConstructorWithoutSuper() {
		System.out.println("**\tMySubclassWithConstructorWithoutSuper() constructor");
	}
	
	
	public MySubclassWithConstructorWithoutSuper(int v) {
		System.out.println("**\tMySubclassWithConstructorWithoutSuper(int) constructor");
	}
	

	public static void main(String[] args) {
		MySubclassWithConstructorWithoutSuper myInstance = new MySubclassWithConstructorWithoutSuper();
		System.out.println();
		MySubclassWithConstructorWithoutSuper myOtherInstance = new MySubclassWithConstructorWithoutSuper(3);
	}

}
