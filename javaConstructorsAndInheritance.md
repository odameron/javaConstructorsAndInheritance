---
title: Java constructors and inheritance
tags: teaching, programming, Java, constructor, inheritance
version: 1.0
date: 2023-08-22
---

> **Summary:**
> - *Principle1:* Java calls the right constructor
> - *Principle2:* A class may not have any (explicit) constructor; Java provides a default (implicit) one without parameter and empty
> - *Principle3:* If a class provides at least one explicit constructor, the implicit one is not available
> - *Principle4:* The constructors of a class are not inherited by its subclasses
> - *Principle5:* Any constructor of a class automatically calls the constructor without parameter of the superclass (unless Principle5)
> - *Principle6:* If a constructor of a class needs to call a constructor with parameter(s) of its superclass, this should be the 1st instruction (and the constructor without parameter of the superclass is not called)


# Principle1: Java calls the right constructor

See file [example/MyClass.java](example/MyClass.java)

```java
public class MyClass {

	public MyClass() {
		System.out.println("*\tMyClass() constructor");
	}
	
	public MyClass(int v) {
		System.out.println("*\tMyClass(int) constructor(int)");
	}

	public static void main(String[] args) {
		MyClass myInstance = new MyClass();
		System.out.println();
		MyClass myOtherInstance = new MyClass(3);
	}

}
```

Results in:

```bash
*	MyClass() constructor

*	MyClass(int) constructor(int)
```

> **Interpretation:**
> - Without surprise, the initialization of `myInstance` and `myOtherInstance` respectively call `MyClass()` and `MyClass(int v)`
> - This demonstrates that by default, `MyClass(int v)` does not call `MyClass()`


# Principle2: A class may not have any (explicit) constructor; Java provides a default (implicit) one without parameter and empty

See file [example/MyClassWithoutConstructor.java](example/MyClassWithoutConstructor.java)

```java
public class MyClassWithoutConstructor {

	public static void main(String[] args) {
		MyClassWithoutConstructor myInstance = new MyClassWithoutConstructor();
		System.out.println();
		
		/*
		 * Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 * The constructor MyClassWithoutConstructor(int) is undefined
		 */
		//MyClassWithoutConstructor myOtherInstance = new MyClassWithoutConstructor(3);
	}

}
```

> **Interpretation:**
> - The initialization of `myInstance` calls `MyClassWithoutConstructor()` which is not defined explicitly in the class
> - Because the class `MyClassWithoutConstructor` does not provide any constructor, Java provides an implicit constructor without parameter that is empty (equivalent to `public MyClassWithoutConstructor() {}`)


# Principle3: If a class provides at least one explicit constructor, the implicit one is not available

See file [example/MyClassWithOneConstructor.java](example/MyClassWithOneConstructor.java)

```java
public class MyClassWithOneConstructor {

	public MyClassWithOneConstructor(int v) {
		System.out.println("*\tMyClassWithOneConstructor(int) constructor(int)");
	}

	public static void main(String[] args) {
		/*
		 * Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 * The constructor MyClassWithOneConstructor() is undefined
		 */
		//MyClassWithOneConstructor myInstance = new MyClassWithOneConstructor();
		
		System.out.println();
		MyClassWithOneConstructor myOtherInstance = new MyClassWithOneConstructor(3);
	}

}
```

Results in:

```bash
*	MyClassWithOneConstructor(int) constructor(int)
```

> **Interpretation:**
> - The initialization of `myOtherInstance` works according to *Principle1*
> - However, the initalization of `myInstance` with the implicit constructor without argument (cf. *Principle2*) is not possible anymore
> - 

# Principle4: The constructors of a class are not inherited by its subclasses

See file [example/MySubclassWithoutConstructor.java](example/MySubclassWithoutConstructor.java) 

```java
public class MySubclassWithoutConstructor extends MyClass {


	public static void main(String[] args) {
		
		MySubclassWithoutConstructor myInstance = new MySubclassWithoutConstructor();
		System.out.println();
		
		/*
		 *  Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 *  The constructor MySubclassWithoutConstructor(int) is undefined
		 */
		//MySubclassWithoutConstructor myOtherInstance = new MySubclassWithoutConstructor(3);

	}

}
```

Results in:

```bash
*	MyClass() constructor
```

> **Interpretation:**
> - The initialization of `myInstance` calls the implicit empty constructor without parameter `MySubclassWithoutConstructor()` (cf. *Principle2*)
> - Because the constructor  `MySubclassWithoutConstructor(int v)` is not defined, `myOtherInstance` cannot be initialized, even though the superclass has a `MyClass(int v)` constructor


# Principle5: Any constructor of a class automatically calls the constructor without parameter of the superclass (unless Principle6)

Here we create several subclasses of `MyClass` (which has two explicit constructors `MyClass()` and `MyClass(int v)`) and initialize instances of these subclasses:

- [example/MySubclassWithoutConstructor.java](example/MySubclassWithoutConstructor.java) does not have any constructor
- [example/MySubclassWithConstructorWithoutSuper.java](example/MySubclassWithConstructorWithoutSuper.java) has two constructors `MySubclassWithConstructorWithoutSuper()` and `MySubclassWithConstructorWithoutSuper(int v)` that override the constructors of `MyClass`

```java
public class MySubclassWithoutConstructor extends MyClass {


	public static void main(String[] args) {
		
		MySubclassWithoutConstructor myInstance = new MySubclassWithoutConstructor();
		System.out.println();
		
		/*
		 *  Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 *  The constructor MySubclassWithoutConstructor(int) is undefined
		 */
		//MySubclassWithoutConstructor myOtherInstance = new MySubclassWithoutConstructor(3);

	}

}
```

Results in:

```bash
*	MyClass() constructor
```

and 

```java
public class MySubclassWithConstructorWithoutSuper extends MyClass {

	public MySubclassWithConstructorWithoutSuper() {
		System.out.println("**\tMySubclassWithConstructorWithoutSuper() constructor");
	}
	
	
	public MySubclassWithConstructorWithoutSuper(int v) {
		System.out.println("**\tMySubclassWithConstructorWithoutSuper(int) constructor");
	}
	

	public static void main(String[] args) {
		MySubclassWithConstructorWithoutSuper myInstance = new MySubclassWithConstructorWithoutSuper();
		System.out.println();
		MySubclassWithConstructorWithoutSuper myOtherInstance = new MySubclassWithConstructorWithoutSuper(3);
	}

}
```

Results in:

```bash
*	MyClass() constructor
**	MySubclassWithConstructorWithoutSuper() constructor

*	MyClass() constructor
**	MySubclassWithConstructorWithoutSuper(int) constructor
```

> **Interpretation:**
> - The principle holds for:
> 	- the implicit default constructor
> 	- the explicit constructor without parameter
> 	- explicit constructors with parameters
> - for `MySubclassWithoutConstructor`
> 	- The initialization of `myInstance` calls the implicit empty constructor without parameter `MySubclassWithoutConstructor()` (cf. *Principle2*)
> 	- This constructor automatically calls the constructor without parameter of the superclass
> 	- As seen with *Principle4*, because the constructor  `MySubclassWithoutConstructor(int v)` is not defined and is not inherited from the superclass, `myOtherInstance` cannot be initialized (compare with `MySubclassWithConstructorWithoutSuper` below)
> - for `MySubclassWithConstructorWithoutSuper`
> 	- both constructors `MySubclassWithConstructorWithoutSuper()` and `MySubclassWithConstructorWithoutSuper(int v)` 
> 		- call the constructor without parameter of the superclass
> 		- do so *before* executing their 1st instruction
> 	- note that `MySubclassWithConstructorWithoutSuper(int v)`
> 		- calls `MyClass()` 
> 		- but does not call `MyClass(int v)`

# Principle6: If a constructor of a class needs to call a constructor with parameter(s) of its superclass, this should be the 1st instruction (and the constructor without parameter of the superclass is not called)

See file [example/MySubclassWithConstructorWithSuper.java](example/MySubclassWithConstructorWithSuper.java) 

```java
public class MySubclassWithConstructorWithSuper extends MyClass {

	
	public MySubclassWithConstructorWithSuper() {
		// call to super() implicit
		System.out.println("**\tMySubclassWithConstructorWithSuper() constructor");
	}
	
	
	public MySubclassWithConstructorWithSuper(int v) {
		/*
		 * here the explicit call to super(v) replaces
		 * the implicit call to super()
		 */
		super(v);
		System.out.println("**\tMySubclassWithConstructorWithSuper(int) constructor");
	}


	public static void main(String[] args) {
		MySubclassWithConstructorWithSuper myInstance = new MySubclassWithConstructorWithSuper();
		System.out.println();
		MySubclassWithConstructorWithSuper myOtherInstance = new MySubclassWithConstructorWithSuper(3);

	}

}
```

Results in:

```bash
*	MyClass() constructor
**	MySubclassWithConstructorWithSuper() constructor

*	MyClass(int) constructor(int)
**	MySubclassWithConstructorWithSuper(int) constructor
```

> **Interpretation:**
> Contrary to `MySubclassWithConstructorWithoutSuper(int v)`, `MySubclassWithConstructorWithSuper(int v)` calls `MyClass(int v)`, and therefore `MyClass()` is not called
